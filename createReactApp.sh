#!/bin/bash

npx create-react-app $1
cp Makefile Dockerfile docker-compose.yml $1

echo -e "\n\n\n\nCreate React App successfully completed\n\n Go to ${1} to run your application\n\n\n\n"
